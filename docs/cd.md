
# Setup CD Project

## Update deployment-values.yaml

Within the CD project there is a spring-music folder with 4 folders there in:

- common
- dev
- test
- prod

Within the dev, test, prod folder there is environment specific config file named deployment-values.yaml.  You will need to edit these files for your environment:

```yaml

#@data/values
---
app_name: spring-music
container_port: 8080
service_port: 80
registry: "harbor.<your domain>/spring-music"
image_name: "master" 
tag: "<tag of your image from harbor>"
service_type: ClusterIP
namespace: dev
replicas: 1
gitlab_project: <URL from your pipeline trigger>
ci_token: <CI token from your pipeline trigger>>
root_dns_name: "<your domain>"
db:
  cpu: "0.4"
  memory: 800Mi

```

## Update the Kapp CR in the CD Project

You must set the URL of your spring-music git repo in the kapp CR definitions in the kapp folder of the CD Project:

```yaml

apiVersion: kappctrl.k14s.io/v1alpha1
kind: App
metadata:
  name: spring-music-dev
  namespace: kapp-gitops-mgmt
spec:
  serviceAccountName: kapp-sa
  fetch:
  - git:
      url: https://<your forked spring music git project>
      ref: origin/dev
      subPath: spring-music
  template:
    - ytt:
        ignoreUnknownComments: true
        paths:
        - common
        - dev
    - kbld: {}
  deploy:
    - kapp:
        intoNs: dev

```
