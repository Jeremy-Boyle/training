# Setup GitLab CI

In your forked Spring Music project go into Settings > CI/CD

![Example](/docs/cicd.png)

Then click on *Runners* and then *Install GitLab Runner on Kubernetes*

![Example](/docs/install-runner.png)

click *Integrate with a Kubernetes Cluster*

![Example](/docs/k8s.png)

click on *Connect existing cluster*

![Example](/docs/existing-clust.png)

Give the cluster a meaningful name and leave the *Environment Scope* *

From the dependencies folder in this repo run `./gitlab-info.sh` and populate the config. You will need to provide:
* Kubernetes Cluster Name
* API URL
* CA Certificat
* Enter new Service Token

(leave the rest set to default)

click *Add Kubernetes Cluster*

![Example](/docs/config.png)

You are connected, deploy a runner

Click on *Appplications* under you newly configured cluster (you should see the name you gave it as the title)

![Example](/docs/applications.png)

Click install on GitLab Runner, this is all you need for now.  It will take minute to deploy the runner out.

![Example](/docs/installer.png)

Go back to *Settings > CI/CD > Runners* and validate your runner is online, **turn off shared runners*

![Example](/docs/installed-runner.png)


Our CI Process uses secrets to configure out pipeline to your environment.

Create 3 secrets by clicking *Settings > CI/CD > Variables* in your Spring Music project

You will find the following script in the dependencies folder of this repo, run `./k8s-connect.sh` copy the output of this command.

- make a variable of type **File** with a name(key) that maps to a description of the cluster (e.g. AWS_DEMO_CLUSTER1) and copy the contents of the script output into the value

- Now create a variable with the name (key) `build_cluster_context` and set the value to the name (key) of the previous file variable.

- Create a spring-music project in your harbor instance.

- Now create a variable with the name (key) `EXT_REGISTRY_IMAGE` and enter your harbor.{your ingress domain}/spring-music.  

![Example](/docs/vars.png)

# Add A Pipeline Trigger

We are gonna use a pipeline trigger so that our CD process can notify us when an application is deployed.

In your forked Spring Music project go into Settings > CI/CD

Click *Pipeline Triggers*

Enter a trigger name, something like *deployment_trigger*

This will create a CI Token and you will find the Pipeline URL (https://gitlab.com/api/v4/projects/25843554/trigger/pipeline)

You will need to plug these values into your CD process later.



